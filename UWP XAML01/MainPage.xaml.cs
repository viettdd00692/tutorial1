﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace UWP_XAML01
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void Grid_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Grid));
        }

        private void Stack_Panel_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(StackPanel));
        }

        private void Canvas_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Canvas));
        }

        private void Scroll_Viewer_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ScrollViewer));
        }

        private void Wrap_Grid_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(WrapGrid));
        }

        private void Relative_Panel_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(RelativePanel));
        }
    }
}
